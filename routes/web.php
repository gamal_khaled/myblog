<?php

use App\Http\Controllers\aboutController;
use App\Http\Controllers\ContactController ;
use App\Http\Controllers\homecontroller;
use App\Http\Controllers\PostController;
use App\Models\post;
use Illuminate\Support\Facades\Route;
use \App\Models\category;
use \App\Http\Controllers\CategoryController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homecontroller::class,'index'])->name('first');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('admin.dashboard');
})->name('dashboard');





Route::get('contact/show',[ContactController::class,'show'])->name('contact.show');

Route::post('message/store',[ContactController::class,'messagestore'])->name('message.store');

Route::get('about',[aboutController::class,'show'])->name('about.show');

Route::get('category/show/{id}',[CategoryController::class,'show'])->name('category.show');

Route::get('single/post/{id}',[postController::class,'show'])->name('single.post');

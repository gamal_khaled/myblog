<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\TagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the Route'ServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//-------- all routes for backend -----------//


 //routs for owner to see all users and control the roles

route::middleware(['auth:sanctum', 'verified'])->prefix('user')->group(function (){
    Route::get('/show',[UserController::class,'index'])->name('user.show');
    Route::get('/edit/{id}',[UserController::class,'edit'])->name('user.edit');
    Route::put('/store/{id}',[ UserController::class,'store'])->name('user.update');
    Route::get('/delete/{id}',[UserController::class,'delete'])->name('user.delete');
});


//routes for categories
Route::middleware(['auth:sanctum', 'verified'])->prefix('category')->group(function (){
    Route::get('/index',[ CategoryController::class , 'index'] )->name('category.index');
    Route::get('/create',[ CategoryController::class,'create'])->name('category.create');
    Route::post('/store',[ CategoryController::class,'store' ])->name('category.store');
    Route::get('/edit/{id}',[ CategoryController::class,'edit' ])->name('category.edit');
    Route::put('/update/{id}',[ CategoryController::class,'update' ])->name('category.update');
    Route::get('/delete/{id}',[ CategoryController::class,'delete' ])->name('category.delete');

});

//routes for posts
Route::middleware(['auth:sanctum', 'verified'])->prefix('post')->group(function (){
    Route::get('/index',[ PostController::class , 'index'] )->name('post.index');
    Route::get('/create',[ PostController::class,'create'])->name('post.create');
    Route::post('/store',[ PostController::class,'store' ])->name('post.store');
    Route::get('/edit/{id}',[ PostController::class,'edit' ])->name('post.edit');
    Route::put('/update/{id}',[ PostController::class,'update' ])->name('post.update');
    Route::get('/delete/{id}',[ PostController::class,'delete' ])->name('post.delete');
});


//tags routs
Route::middleware(['auth:sanctum', 'verified'])->prefix('tags')->group(function (){
    Route::get('/index',[ TagController::class , 'index'] )->name('tag.index');
});
//contact routs
Route::middleware(['auth:sanctum', 'verified'])->prefix('contact')->group(function (){
Route::get('/index',[ContactController::class,'index'])->name('contact.index');
Route::get('/create',[ContactController::class,'create'])->name('contact.create');
Route::post('/store',[ContactController::class,'store'])->name('contact.store');
Route::get('/delete/{id}',[ContactController::class,'delete'])->name('contact.delete');
});

//contact form routes
Route::middleware(['auth:sanctum', 'verified'])->prefix('contactMessages')->group(function (){
    Route::get('showMessages',[ContactController::class,'messages'])->name('contact.messages');
    Route::get('message/delete/{id}',[ContactController::class,'messagedelete'])->name('message.delete');
});

Route::middleware(['auth:sanctum', 'verified'])->prefix('about')->group(function (){
    Route::get('/index',[AboutController::class,'index'])->name('about.index');
    Route::get('/create',[AboutController::class,'create'])->name('about.create');
    Route::post('/store',[AboutController::class,'store'])->name('about.store');
    Route::get('/delete/{id}',[AboutController::class,'destroy'])->name('about.delete');
});






































<div class="header__top">
    <div class="header__logo">
        <a class="site-logo" href="{{route('first')}}">
            <img src="{{ asset('frontend/images/logo.svg') }}" alt="Homepage">
        </a>
    </div>

    <div class="header__search">

        <form role="search" method="get" class="header__search-form" action="#">
            <label>
                <span class="hide-content">Search for:</span>
                <input type="search" class="header__search-field" placeholder="Type Keywords" value="" name="s" title="Search for:" autocomplete="off">
            </label>
            <input type="submit" class="header__search-submit" value="Search">
        </form>

        <a href="#0" title="Close Search" class="header__search-close">Close</a>

    </div>  <!-- end header__search -->

    <!-- toggles -->
    <a href="#0" class="header__search-trigger"></a>
    <a href="#0" class="header__menu-toggle"><span>Menu</span></a>

</div> <!-- end header__top -->

<nav class="header__nav-wrap">

    <ul class="header__nav">
        <li class="current"><a href="{{route('first')}}" title="">Home</a></li>
        <li class="has-children">
            <a href="#0" title="">Categories</a>
            <ul class="sub-menu">
                @foreach($allcat as $cat)
                <li><a href="{{ route('category.show',$cat->id) }}">{{$cat->category_name}}</a></li>
                @endforeach
            </ul>
        </li>


        <li><a href="{{route('about.show')}}" title="">About</a></li>
        <li><a href="{{route('contact.show')}}" title="">Contact</a></li>
        @if(!Auth::check())
        <li><a href="{{route('login')}}" title="">login</a></li>
        <li><a href="{{route('register')}}" title="">register</a></li>
        @endif
        @if(Auth::check())
            <li><a href="{{route('dashboard')}}" title="">dashboard</a></li>

        @endif

    </ul> <!-- end header__nav -->

    <ul class="header__social">
        <li class="ss-facebook">
            <a href="https://facebook.com/">
                <span class="screen-reader-text">Facebook</span>
            </a>
        </li>
        <li class="ss-twitter">
            <a href="#0">
                <span class="screen-reader-text">Twitter</span>
            </a>
        </li>
        <li class="ss-dribbble">
            <a href="#0">
                <span class="screen-reader-text">Dribbble</span>
            </a>
        </li>
        <li class="ss-pinterest">
            <a href="#0">
                <span class="screen-reader-text">Behance</span>
            </a>
        </li>
    </ul>

</nav> <!-- end header__nav-wrap -->

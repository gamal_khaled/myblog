@extends('website.layout.master')
@section('content')
    <main class="row content__page">

        <section class="column large-full entry format-standard">

            <div class="media-wrap">
                <div>
                    <img src="{{asset('frontend/images/thumbs/about/about-1000.jpg')}}" srcset="{{asset('frontend/images/thumbs/about/about-2000.jpg')}} 2000w,
                                      {{asset('frontend/images/thumbs/about/about-1000.jpg')}} 1000w,
                                      {{asset('frontend/images/thumbs/about/about-500.jpg ')}}500w" sizes="(max-width: 2000px) 100vw, 2000px" alt="">
                </div>
            </div>

            <div class="content__page-header">
                <h1 class="display-1">
                    {{$about->title}}
                </h1>
            </div> <!-- end content__page-header -->

            <p class="lead drop-cap">
                {{$about->description}}
            </p>



            <h2>This Is Our Story</h2>

            <p>
               {{$about->story}}

            </p>

            <hr>



        </section>

    </main>
@endsection

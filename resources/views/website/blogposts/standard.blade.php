@extends('website.layout.master')
@section('content')

    <main class="row content__page">


        <article class="column large-full entry format-standard">
                @if($post->image!==null)
            <div class="media-wrap entry__media">
                <div class="entry__post-thumb">
                    <img src="{{asset($post->image)}}"
                         srcset="{{asset($post->image)}} 2000w,
                                         {{asset($post->image)}} 1000w,
                                         {{asset($post->image)}} 500w" sizes="(max-width: 2000px) 100vw, 2000px" alt="">
                </div>
            </div>
            @endif
            <div class="content__page-header entry__header">
                <h1 class="display-1 entry__title">
                    {{$post->title}}
                </h1>
                <ul class="entry__header-meta">
                    <li class="author">By <span>{{$post->user->name}}</span></li>
                    <li class="date">{{$post->created_at->diffforhumans()}}</li>

                </ul>
            </div> <!-- end entry__header -->

            <div class="entry__content">

                <p class="lead drop-cap">
                  {{$post->description}}
                </p>





                <p class="entry__tags">
                    <span>Post Tags</span>

                    <span class="entry__tag-list">
                        @foreach($post->tagNames() as $tag)
                                <a >{{$tag}}</a>
                        @endforeach
                            </span>

                </p>
            </div> <!-- end entry content -->

            <div class="entry__pagenav">
                    @if($post->id>1)
                <div class="entry__prev">

                    <a href="{{route('single.post',$post->id-1)}}" rel="prev">
                        <span>Previous Post</span>

                    </a>
                </div>
                @endif
                @if($post->id+1!==null)
                <div class="entry__next">
                    <a href="{{route('single.post',$post->id+1)}}" rel="next">
                        <span>Next Post</span>

                    </a>
                </div>
                  @endif
                <div class="entry__nav">

                </div>
            </div> <!-- end entry__pagenav -->

            <div class="entry__related">
                <h3 class="h2">Related Articles</h3>

                <ul class="related">
                    <li class="related__item">
                        <a href="single-standard.html" class="related__link">
                            <img src="images/thumbs/masonry/walk-600.jpg" alt="">
                        </a>
                        <h5 class="related__post-title">Using Repetition and Patterns in Photography.</h5>
                    </li>
                    <li class="related__item">
                        <a href="single-standard.html" class="related__link">
                            <img src="images/thumbs/masonry/dew-600.jpg" alt="">
                        </a>
                        <h5 class="related__post-title">Health Benefits Of Morning Dew.</h5>
                    </li>
                    <li class="related__item">
                        <a href="single-standard.html" class="related__link">
                            <img src="images/thumbs/masonry/rucksack-600.jpg" alt="">
                        </a>
                        <h5 class="related__post-title">The Art Of Visual Storytelling.</h5>
                    </li>
                </ul>
            </div> <!-- end entry related -->

        </article> <!-- end column large-full entry-->



    </main>

@endsection

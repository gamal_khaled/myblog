@extends('website.layout.master')
@section('content')

    <main class="row content__page">

        <section class="column large-full entry format-standard">

            <div class="media-wrap">
                <div>
                    <img src="{{asset('frontend/images/thumbs/contact/contact-1000.jpg')}}" srcset="{{asset('frontend/images/thumbs/contact/contact-2000.jpg')}} 2000w,
                                      {{asset('frontend/images/thumbs/contact/contact-1000.jpg')}} 1000w,
                                      {{asset('frontend/images/thumbs/contact/contact-500.jpg')}} 500w" sizes="(max-width: 2000px) 100vw, 2000px" alt="">
                </div>
            </div>

            <div class="content__page-header">
                <h1 class="display-1">
                   {{$contactinfo->title}}
                </h1>
            </div> <!-- end content__page-header -->

            <p class="lead drop-cap">
                {{$contactinfo->description}}
            </p>



            <div class="row">
                <div class="column large-six tab-full">
                    <h4>Where to Find Us</h4>

                    <p>
                        {{$contactinfo->address}}
                    </p>

                </div>

                <div class="column large-six tab-full">
                    <h4>Contact Info</h4>

                    <p>{{$contactinfo->email}}<br>
                        info@typerite.com <br>
                        Phone:{{$contactinfo->phone}}
                    </p>

                </div>
            </div>

            <h3 class="h2">Say Hello</h3>

            <form name="contactForm" id="contactForm" method="post" action="{{route('message.store')}}" autocomplete="off">
                @csrf
                <fieldset>

                    <div class="form-field">
                        <input name="name" id="cName" class="full-width" placeholder="Your Name" value="" type="text">
                        @error('name')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="form-field">
                        <input name="email" id="cEmail" class="full-width" placeholder="Your Email" value="" type="text">
                        @error('email')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="message form-field">
                        <textarea name="message" id="cMessage" class="full-width" placeholder="Your Message"></textarea>
                        @error('message')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <input  id="submit" class="btn btn--primary btn-wide btn--large full-width" value="Send Message" type="submit">

                </fieldset>
            </form> <!-- end form -->

        </section>

    </main>


@endsection

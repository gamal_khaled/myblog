@extends('website.layout.master')
@section('content')
    <header class="listing-header">
        <h1 class="h2">Category: {{$cat->category_name}} </h1>
    </header>


    <div class="masonry-wrap">

        <div class="masonry">

            <div class="grid-sizer"></div>

            @foreach($cat->posts as $post)
                <article class="masonry__brick entry format-standard animate-this">
                    @if($post->image !== null)
                        <div class="entry__thumb">
                            <a href="" class="entry__thumb-link">
                                <img src="{{asset($post->image)}}"
                                     srcset="{{asset($post->image)}}" width="393.062" height="300" alt="">
                            </a>
                        </div>
                    @endif
                    <div class="entry__text">
                        <div class="entry__header">

                            <h2 class="entry__title"><a href="{{route('single.post',$post->id)}}">{{$post->title}}</a></h2>
                            <div class="entry__meta">
                                    <span class="entry__meta-cat">
                                        @foreach($post->tagNames() as $tag)
                                            <h6 style="display: inline-block;font-size:11.6px ">{{$tag.' , '}}</h6>
                                        @endforeach
                                    </span>
                                <span class="entry__meta-date">
                                        <h6 style="display: inline-block;font-size:11.6px ">{{$post->created_at->diffForHumans()}}</h6>
                                    </span>
                            </div>

                        </div>
                        <div class="entry__excerpt">
                            <p>
                                {{$post->description}}
                            </p>
                        </div>
                    </div>

                </article> <!-- end article -->
            @endforeach

        </div> <!-- end masonry -->

    </div> <!-- end masonry-wrap -->


@endsection

@extends('admin.layouts.app')
@section('content')

    <div class="container mt-5">
        <form action="{{route('tag.store')}}" method="post">
            @csrf
            <div class="mb-3">
                <label  for="exampleInputEmail1" class="form-label">Create Tag</label>
                <input type="text" name='tag_name'  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    @error('tag_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
            </div>
            <button type="submit" class="btn btn-primary">create</button>
        </form>
    </div>

    @endsection

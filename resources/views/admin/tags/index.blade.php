@extends('admin.layouts.app')

@section('content')
    <div class="container mt-5">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">num</th>
                <th scope="col">tag</th>
                <th scope="col">related posts</th>

            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($tags as $tag )
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$tag->name}}</td>
                <td>{{$tag->count}}</td>

            </tr>
            @endforeach
            </tbody>
        </table>
    </div>


@endsection

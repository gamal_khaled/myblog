@extends('admin.layouts.app')

@section('content')
    <div class="container mt-5">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">category</th>
                <th scope="col">related posts </th>
                <th scope="col">created at</th>
                <th scope="col">action</th>
            </tr>
            </thead>
            <tbody>
            @php
            $i=1;
            @endphp
            @foreach($category as $cat)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$cat->category_name}}</td>
                <td></td>
                <td>{{$cat->created_at->diffforhumans()}}</td>
                <td>
                    <a href="{{route('category.edit',$cat->id)}}" class="btn btn-primary">Edit</a>
                    <a href="{{route('category.delete',$cat->id)}}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @endsection

@extends('admin.layouts.app')

@section('content')
    <div class="container mt-5">
        <form method="post" action="{{route('category.update',$category->id)}}">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label style="position: center" for="exampleInputEmail1" class="form-label">Category Name</label>
                <input type="text" name="category_name" value="{{$category->category_name}}"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection

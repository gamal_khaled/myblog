@extends('admin.layouts.app')

@section('content')
    <div class="container mt-5">
        <form method="post" action="{{route('category.store')}}">
            @csrf
            <div class="mb-3">
                <label style="position: center" for="exampleInputEmail1" class="form-label">Category Name</label>
                <input type="text" name="category_name"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    @endsection

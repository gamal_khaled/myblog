@extends('admin.layouts.app')
@section('content')

    <div class="container">

        <a href="{{route('about.create')}}" class="btn btn-primary m-3">Create About us</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">description</th>
                <th scope="col">story</th>
                <th scope="col">Action</th>

            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($abouts as $about)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$about->title}}</td>
                    <td>{{$about->description}}</td>
                    <td>{{$about->story}}</td>

                    <td>
                        <a href="{{route('about.delete',$about->id)}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">email</th>
                <th scope="col">created at</th>

                <th scope="col">action</th>

            </tr>
            </thead>
            <tbody>
                @php
                $i=1;
                @endphp
            @foreach($users as $user)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at->diffforhumans()}}</td>

                <td>
                    <a href="{{route('user.edit',['id'=>$user->id])}}" class="btn btn-primary">Edit</a>
                    <a href="{{route('user.delete',$user->id)}}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>



@endsection






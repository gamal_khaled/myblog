@extends('admin.layouts.app')
@section('content')

    <div class="container mt-3">
        <form action="{{ route('user.update',$user->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">name</label>
                <input type="text" name="name" value="{{$user->name}}" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" name="email" value="{{$user->email}}" disabled id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" name="role[]" value="owner" {{$user->hasRole('owner')?'checked':''}} id="exampleCheck1">
                <label class="form-check-label"  for="exampleCheck1">owner</label>
            </div>
            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1" value="admin" name="role[]" {{$user->hasrole('admin')?'checked':''}}>
                <label class="form-check-label" for="exampleCheck1">admin</label>
            </div>
            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" value="user" name="role[]" {{$user->hasrole('user')?'checked':''}} id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">user</label>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    @endsection

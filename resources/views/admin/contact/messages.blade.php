@extends('admin.layouts.app')
@section('content')

    <div class="container">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">email</th>
                <th scope="col">message</th>
                <th scope="col">Action</th>


            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($contactMessage as $message)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$message->name}}</td>
                    <td>{{$message->email}}</td>
                    <td>{{$message->message}}</td>
                    <td>
                        <a href="{{route('message.delete',$message->id)}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

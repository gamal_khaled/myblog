@extends('admin.layouts.app')
@section('content')
    <div class="container mt-5">
        <form method="post" action="{{route('contact.store')}}">
            @csrf
            <div class="mb-3">
                <label style="position: center" for="exampleInputEmail0" class="form-label">title</label>
                <input type="text" name="title"  class="form-control" id="exampleInputEmail0" aria-describedby="emailHelp">
                        @error('title')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label style="position: center" for="exampleInputEmail1" class="form-label">Description</label>
                <textarea name="description"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"></textarea>
                @error('description')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label style="position: center" for="exampleInputEmail2" class="form-label">address</label>
                <input type="text" name="address"  class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp">
                @error('address')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label style="position: center" for="exampleInputEmail3" class="form-label">Email</label>
                <input type="email" name="email"  class="form-control" id="exampleInputEmail3" aria-describedby="emailHelp">
                @error('email')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="mb-3">
                <label style="position: center" for="exampleInputEmail4" class="form-label">phone</label>
                <input type="text" name="phone"  class="form-control" id="exampleInputEmail4" aria-describedby="emailHelp">
                @error('phone')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>



            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection

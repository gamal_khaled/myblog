@extends('admin.layouts.app')
@section('content')

    <div class="container">

        <a href="{{route('contact.create')}}" class="btn btn-primary m-3">Create Info</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">description</th>
                <th scope="col">address</th>
                <th scope="col">email</th>
                <th scope="col">phone</th>
                <th scope="col">Action</th>

            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($contact as $contactinfo)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$contactinfo->title}}</td>
                    <td>{{$contactinfo->description}}</td>
                    <td>{{$contactinfo->address}}</td>
                    <td>{{$contactinfo->email}}</td>
                    <td>{{$contactinfo->phone}}</td>
                    <td>
                        <a href="{{route('contact.delete',$contactinfo->id)}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@extends('admin.layouts.app')
@section('content')
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">title</th>
                <th scope="col">description</th>
                <th scope="col">category</th>
                <th scope="col">image</th>
                <th scope="col">action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
                @endphp
            @foreach($posts as $post)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->description}}</td>
                <td>{{$post->category->category_name}}</td>
                <td><img src="{{asset($post->image)}}" height="50"width="50" alt="image"></td>
                <td>
                    <a href="{{route('post.edit',$post->id)}}" class="btn btn-primary">Edit</a>
                    <a href="{{route('post.delete',$post->id)}}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@extends('admin.layouts.app')

@section('links')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
@endsection

@section('content')
    <div class="container mt-5" >
        <form method="post" action=" {{route('post.update',$post->id)}} " enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="old_image" value="{{$post->image}}">
            <input type="hidden" name="old_tags[]" value="
            @foreach ($post->tagNames() as $tag)
                {{$tag .' , '}}
            @endforeach
            ">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">title</label>
                <input type="text" name="title" value="{{$post->title}}"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                @error('title')
                <span class="text-danger">
                    {{$message}}
                </span>
                @enderror
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">description</label>
                <textarea name="description" class="form-control" id="exampleInputPassword1"> {{$post->description}} </textarea>
                @error('description')
                <span class="text-danger">
                    {{$message}}
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <label class="input-group-text" for="inputGroupFile01">Image</label>
                <input type="file"  name="image" class="form-control" id="inputGroupFile01">
                @error('image')
                <span class="text-danger">
                    {{$message}}
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <label class="input-group-text" for="inputGroupFile01">category</label>
                <select class="form-control" name="category_id" aria-label="Default select example">
                    @foreach($category as $cat )
                        <option value="{{$cat->id}}">{{$cat->category_name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="form-group mb-3">
                <label class="input-group-text" for="tags">Tags</label>
                <input type="text" name="tags[]"  value="
                               @foreach ($post->tagNames() as $tag)
                                            {{$tag .' , '}}
                                            @endforeach
    " class="form-control" data-role="tagsinput" id="tags" aria-describedby="emailHelp">

            </div>

            <button type="submit" class="btn btn-primary">edit post</button>
        </form>

    </div>
@endsection

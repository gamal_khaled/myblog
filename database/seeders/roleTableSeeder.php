<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;


class roleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = Role::create([
            'name' => 'owner',
            'display_name' => 'Project Owner',
            'description' => 'User is the owner of a given project',
        ]);

        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'User Administrator',
            'description' => 'User is allowed to manage and edit other users',
        ]);

        $user = Role::create([
            'name' => 'user',
            'display_name' => 'user',
            'description' => 'User is allowed to make posts and comments',
        ]);
    }
}

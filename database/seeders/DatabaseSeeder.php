<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(10)->create();
         \App\Models\category::factory(7)->create();
         \App\Models\post::factory(20)->create();
        \App\Models\about::factory(1)->create();
        $this->call(roleTableSeeder::class);
        $this->call(userTableSeeder::class);

    }
}

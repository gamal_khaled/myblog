<?php

namespace Database\Factories;

use App\Models\contact;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class contactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'description' => $this->faker->paragraph($nbSentences = 6, $variableNbSentences = true),
            'address'=>$this->faker->address,
            'email'=>$this->faker->email,
            'phone'=>$this->faker->phoneNumber,
        ];
    }
}

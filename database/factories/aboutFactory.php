<?php

namespace Database\Factories;

use App\Models\about;
use Illuminate\Database\Eloquent\Factories\Factory;

class aboutFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = about::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'description' => $this->faker->paragraph($nbSentences = 6, $variableNbSentences = true),
            'story'=> $this->faker->paragraph($nbSentences = 12, $variableNbSentences = true),
        ];
    }
}

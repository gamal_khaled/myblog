<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:owner']);
    }

    public function index()
    {
        $users=user::all();
        return view('admin.users.index' ,compact('users'));
    }

    public function edit($id)
    {
        $user=User::FindOrFail($id);

        return view('admin.users.edit',compact('user'));
    }

    public function store(Request $request, User $user ,$id)
    {
        $request->validate([
            'name'=>'required',
            'role'=>'required|array|min:1'


        ]);
//        dd($request);
        $reqdata=$request->except('email');

        $user->findorfail($id)->update($reqdata);
        $user->findorfail($id)->syncRoles($request->role);

        return redirect()->route('user.show');

    }

    public function delete($id)
    {
        User::findorfail($id)->delete();
         return redirect()->back();

    }
}

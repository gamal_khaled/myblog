<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\post;
use App\Models\tag;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function __construct()
    {
        $allCat=category::all();
        view::share('allcat',$allCat);
    }

    public function index()
    {
        $posts=post::all();

        $tags =post::existingTags();
        return view('admin.posts.index',compact('posts','tags'));

    }

    public function create()
    {
        $category=category::all();

        return view('admin.posts.create' ,compact('category',));

    }

    public function store(Request $request)
    {



        $request->validate([
            'title'=>'min:2|max:100|required|',
            'description'=>'max:1000|required',
            'category_id'=>'required',
            'image'=>' mimes:jpg,jpeg,png',
            'tags'=>'required'
        ]);//validation

        $image=$request->file('image');
        $name_gen= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,300)->save('storage/image/post/'.$name_gen);
        $link_image='storage/image/post/'.$name_gen;

        $post=post::create([
            'user_id'=>Auth::id(),
            'title'=>$request->title,
            'description'=>$request->description,
            'image'=>$link_image,
            'category_id'=>$request->category_id,
            'created_at'=>carbon::now()

        ]);
        $post->tag(explode(',', $request->tags[0]));



        return redirect()->route('post.index');


    }

    public function edit($id)
    {

        $post=post::findorfail($id);
        $category=category::all();
        return view('admin.posts.edit',compact('post','category'));

    }

    public function update(Request $request,$id)
    {
        $validated = $request->validate([

            'title'=>'min:2|max:100|required|',
            'description'=>'min:5|max:1000|required',


        ]);

        $old_img=$request->old_image;
        $image=$request->file('image');
        $old_tags=$request->old_tags;

        $tags=$request->tags[0];

        if ($image && $old_img!==null) {

            $name_gen= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(300,300)->save('storage/image/post/'.$name_gen);
            $link_image='storage/image/post/'.$name_gen;


            unlink($old_img);


            $editedpost=post::findorfail($id)->update([
                'title'=>$request->title,
                'description'=>$request->description,
                'image'=>$link_image,
                'updated_at'=>carbon::now(),

            ]);
            $editedpost->untag();
            $editedpost->tag(explode(',', $request->tags[0]));

            return redirect()->route('post.index');



        }elseif ($image && $old_img==null){


            $name_gen= hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(300,300)->save('storage/image/post/'.$name_gen);
            $link_image='storage/image/post/'.$name_gen;

            $editedpost=post::findorfail($id)->update([
                'title'=>$request->title,
                'description'=>$request->description,
                'image'=>$link_image,
                'updated_at'=>carbon::now(),

            ]);

            $editedpost=post::findorfail($id)->retag(explode(',', $request->tags[0]));

            return redirect()->route('post.index');

        }
        else{


            $editedpost=post::findorfail($id)->update([
                'title'=>$request->title,
                'description'=>$request->description,
                'category_id'=>$request->category_id,
                'created_at'=>Carbon::now()

            ]);
            $editedpost=post::findorfail($id)->retag(explode(',', $request->tags[0]));

            return redirect()->route('post.index');

        }



    }


    public function show($id)
    {
        $post=post::where('id',$id)->first();

        return view('website.blogposts.standard',compact('post'));
    }


    public function delete($id)
    {

        $post=post::findorfail($id);

        if ($post->image){
        $old_img=$post->image;
        unlink($old_img);
        $post->untag();
        post::findorfail($id)->delete();
        return redirect()->route('post.index');

        }else{

        $post->untag();
        post::findorfail($id)->delete();
        return redirect()->route('post.index');
        }




    }

}

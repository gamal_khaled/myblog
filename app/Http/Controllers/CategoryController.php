<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class CategoryController extends Controller
{

    public function index(category $category)
    {
            $category=category::all();
        return view('admin.category.index',compact('category'));


    }

    public function show($id)
    {

        $allCat=category::all();
        view::share('allcat',$allCat);
        $cat=category::where('id',$id)->firstorfail();

        return view('website.category.category',compact('allCat','cat'));

    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request,category $category)
    {
        $request->validate([
            'category_name'=>'required'
        ]);

        $category=category::create([
            'category_name'=>$request->category_name,
            'user_id'=>Auth::user()->id,
        ]);
        return redirect()->route('category.index');



    }

    public function edit($id)
    {
        $category=category::findOrFail($id);
        return view('admin.category.edit',compact('category'));


    }

    public function update(Request $request,category $category,$id)
    {
        $request->validate([
            'category_name'=>'required'
        ]);

        category::findorfail($id)->update([
            'category_name'=>$request->category_name,
        ]);

        return redirect()->route('category.index');


    }

    public function delete($id)
    {
        category::findorfail($id)->delete();

        return redirect()->route('category.index');

    }
}

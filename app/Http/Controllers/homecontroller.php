<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class homecontroller extends Controller
{
    public function index()
    {
        $posts=post::latest()->paginate(5);
        $allCat=category::all();
        view::share('allcat',$allCat);

        return view('website.index',compact('posts'));

    }
}

<?php

namespace App\Http\Controllers;

use App\Models\about;
use App\Models\category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;

class aboutController extends Controller
{

    public function __construct()
    {

        $allCat=category::all();
        view::share('allcat',$allCat);

    }

    public function index()
    {
        $abouts=about::all();

        return view('admin.about.index',compact('abouts'));
    }


    public function create()
    {
        return view('admin.about.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|max:50',
            'description'=>'required|min:25|',
            'story'=>'required',


        ]);

        about::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'story'=>$request->story,

        ]);

        return redirect()->route('about.index');
    }


    public function show()
    {
        $about=about::first();
        return view('website.about.about',compact('about'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        about::findorfail($id)->delete();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\contact;
use App\Models\contactform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ContactController extends Controller
{
    public function __construct()
    {
        $allCat=category::all();
        view::share('allcat',$allCat);


    }


    public function index()
    {
        $contact=contact::all();
        return view('admin.contact.index',compact('contact'));

    }

    public function create()
    {
        return view('admin.contact.create');

    }

    public function store(Request $request)
    {

        $request->validate([
            'title'=>'required|max:50',
            'description'=>'required|min:25|',
            'address'=>'required',
            'email'=>'required',
            'phone'=>'required'

        ]);

        contact::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'address'=>$request->address,
            'email'=>$request->email,
            'phone'=>$request->phone,
        ]);

        return redirect()->route('contact.index');

    }

    public function show()
    {

        $contactinfo=contact::first();
//        dd($contactinfo);

        return view('website.contact.contact',compact('contactinfo'));

    }

    public function delete($id)
    {
        contact::findorfail($id)->delete();
        return redirect()->route('contact.index');

    }

    //-----------------------------------------------------------------------------------//
    //contact form and messages


    public function messages()
    {

        $contactMessage=contactform::all();

        return view('admin.contact.messages',compact('contactMessage'));

    }

    public function messagestore(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'message'=>'required',
        ]);

        contactform::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'message'=>$request->message,
        ]);
        return redirect()->back();

    }



    public function messagedelete($id)
    {
        contactform::findorfail($id)->delete();
        return redirect()->route('contact.messages');

    }

}

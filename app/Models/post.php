<?php

namespace App\Models;

use Conner\Tagging\Taggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use  App\Models\category;
use App\Models\tag;


class post extends Model
{
    use Taggable;
    use HasFactory;

    protected $fillable=[
        'title','description','image','category_id','user_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class,);

    }

    public function category()
    {
        return $this->belongsTo(category::class,);

    }

    public function tags()
    {

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\post;
use \App\Models\User;

class category extends Model
{
    use HasFactory;
    protected $fillable=[
        'category_name',
        'user_id'
    ];

    public function posts(){
         return $this->hasMany(post::class,);
    }

    public function user()
    {
       return $this->belongsTo(User::class);

    }
}
